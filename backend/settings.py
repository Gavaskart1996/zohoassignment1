
import os
import json

# Set settings values from environment variables or env.json file
if os.path.isfile('env.json'):
    with open('env.json') as f:
        data = json.load(f)

        baseurl = data['baseurl']
else:
    baseurl = os.environ['baseurl']