from flask import Flask, jsonify, request
from flask_cors import CORS,cross_origin

# from flask_sqlalchemy import SQLAlchemy
import optparse
import settings

import requests


import click


app = Flask(__name__)


# cors = CORS(app, resources=r'/api/*')

@app.route('/', defaults={'path': ''}, methods=["GET","POST","PUT","DELETE"])
@app.route('/<path:path>', methods=["GET","POST","PUT","DELETE"])
@cross_origin(allow_headers=['*'])
def getauthorizepattern(path):

    '''get url path and method from header'''

    endpoint = str(settings.baseurl)+str(path)

    if request.method == 'GET':
    
        res = requests.get(endpoint)

        return jsonify(res.json()), 200
  
def flaskrun(app, default_host="0.0.0.0", 
                  default_port="8000"):
    """
    Takes a flask.Flask instance and runs it. Parses 
    command-line flags to configure the app.
    """

    # Set up the command-line options
    parser = optparse.OptionParser()
    parser.add_option("-H", "--host",
                      help="Hostname of the Flask app " + \
                           "[default %s]" % default_host,
                      default=default_host)
    parser.add_option("-P", "--port",
                      help="Port for the Flask app " + \
                           "[default %s]" % default_port,
                      default=default_port)

    options, _ = parser.parse_args()

    app.run(
        debug=True,
        host=options.host,
        port=int(options.port)
    )

if __name__ == "__main__":
    flaskrun(app)
