$(function(){

    var api = endpoint+"api/"+mykey+"/"+id+"/";

    console.log(api);

    function updateimage(data){
        $("#profile-picture").attr("src",data.url);
        $("#profile-picture").attr("alt",data.name)
    }

    function updatebiography(data){
        $("#heroname").html(data['full-name']+'<small><i class="fa fa-map-marker"></i> '+data['place-of-birth']+'</small>');
    }

    function updatework(data){
        $("#herowork").html('<i class="fa fa-briefcase"></i> '+data.occupation);
    }

    function updateappearance(data){
        console.log(data);
        $("#gender").html("Gender: "+data.gender);
        $("#race").html("Race: "+data.race);
        $("#heigth-weight").html("Height: "+data.height[1]+" Weight: "+data.weight[1]);
        $("#color").html("Hair Color: "+data["hair-color"]+"Eye Color: "+data["eye-color"]);
    }

    function updatepowers(data){
        $("#combat").attr("title",data.combat);
        $("#durability").attr("title",data.durability);
        $("#intelligence").attr("title",data.intelligence);
        $("#power").attr("title",data.power);
        $("#speed").attr("title",data.speed);
        $("#strength").attr("title",data.strength);
    }

        let searchval = $(this).val();
        $.get(api+'image/',  // url
        function (data, textStatus, jqXHR) {  // success callback
            if (data.response == 'success'){
                updateimage(data);
            }
            
        });

        $.get(api+'biography/',  // url
        function (data, textStatus, jqXHR) {  // success callback
            if (data.response == 'success'){
                updatebiography(data);
            }
            
        });

        $.get(api+'work/',  // url
        function (data, textStatus, jqXHR) {  // success callback
            if (data.response == 'success'){
                updatework(data);
            }
            
        });

        $.get(api+'appearance/',  // url
        function (data, textStatus, jqXHR) {  // success callback
            if (data.response == 'success'){
                updateappearance(data);
            }
            
        });

        $.get(api+'powerstats/',  // url
        function (data, textStatus, jqXHR) {  // success callback
            if (data.response == 'success'){
                updatepowers(data);
            }
            
        });

    var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
    
});