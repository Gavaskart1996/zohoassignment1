$(function(){

    var api = endpoint+"api/"+mykey+"/";

    function updateprofiles(data){


        $("figure").remove(".snip1336");

        htmldata = "<figure class='snip1336'>\
        <img src='https://source.unsplash.com/user/erondu/300x400' alt='sample87' />\
        <figcaption>\
          <img src='IMAGESRC' title='TITLENAME' class='profile' />\
          <h2>FULLNAME</h2>\
          <p>WORK</p>\
          <a href='/HEROID/details' class='info'>More Info</a>\
        </figcaption>\
      </figure>"

        fullhtml = ''

        for (d in data.results){
            profile = htmldata.replace("IMAGESRC",data.results[d].image.url);
            
            profile = profile.replace("FULLNAME",data.results[d].name);
            profile = profile.replace("TITLENAME",data.results[d].name);

            profile = profile.replace("WORK",data.results[d].work.occupation);
            profile = profile.replace("HEROID",data.results[d].id);
            profilehtml = profile;//+"</div>";

            fullhtml = fullhtml + profilehtml;
        }
            
        $("#profile-card").append(fullhtml);
    }

    $("#search").focusout(function(){

        let searchval = $(this).val();
        $.get(api+'search/'+searchval,  // url
        function (data, textStatus, jqXHR) {  // success callback
            if (data.response == 'success'){
                updateprofiles(data);
            }
            
        });
    });
    
});