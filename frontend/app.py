from flask import Flask, jsonify, request,render_template

import optparse

app = Flask(__name__)

@app.route('/', methods=["GET"])
def index():

    return render_template("index.html")

@app.route('/<id>/details',methods=["GET"])
def herodetails(id):

    return render_template("herodetails.html",id=id)

def flaskrun(app, default_host="0.0.0.0", 
                  default_port="8001"):
    """
    Takes a flask.Flask instance and runs it. Parses 
    command-line flags to configure the app.
    """

    # Set up the command-line options
    parser = optparse.OptionParser()
    parser.add_option("-H", "--host",
                      help="Hostname of the Flask app " + \
                           "[default %s]" % default_host,
                      default=default_host)
    parser.add_option("-P", "--port",
                      help="Port for the Flask app " + \
                           "[default %s]" % default_port,
                      default=default_port)

    options, _ = parser.parse_args()

    app.run(
        debug=True,
        host=options.host,
        port=int(options.port)
    )

if __name__ == "__main__":
    flaskrun(app)
